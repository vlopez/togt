<?php

setlocale(LC_ALL,'en_US.utf8');

header("Content-type: text/plain; charset=utf-8");

echo str_repeat("*",72) . "\n";

$fd=tmpfile();
$file = stream_get_meta_data($fd)['uri'];
fwrite($fd, $_REQUEST['INPUT']);
fflush($fd);

(strpos($_REQUEST['OPTIONS'],"+") === FALSE
	&& strpos($_REQUEST['OPTIONS'],"/") === FALSE
	&& strpos($_REQUEST['OPTIONS'],"\\") === FALSE
	&& strpos($_REQUEST['OPTIONS'],"!") === FALSE
	&& strpos($_REQUEST['OPTIONS'],"\"") === FALSE
	&& strpos($_REQUEST['OPTIONS'],"'") === FALSE
        ) or die("Invalid character in options list.\nDid you use one of the following:  +/\\!\"' ?");


passthru('LC_ALL=en_US.utf8 timeout --kill-after=3 60 /var/www/bin/tog-2020.01.22/tog  +RTS -M1.4G -s -RTS 2>&1 ' . $file . ' ' .
        	implode(" ",array_map('escapeshellarg',explode(" ",$_REQUEST['OPTIONS']))), $exit_code);

fclose($fd);

echo str_repeat("*",72) . "\n";

if($exit_code === 0) {
	echo "Type-checking succeeded";
}elseif($exit_code === 142) {
// The documentation of the GNU timeout command states
// that if the program is timed out, then the exit code is
// 124.
	echo "Timed out (60s)";
}else{
	echo "Type-checking failed. Exit code $exit_code";
}
