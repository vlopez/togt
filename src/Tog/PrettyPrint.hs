{-# OPTIONS_GHC -fno-warn-orphans #-}
-- | Small pretty-printing library working on wl-pprint.
module Tog.PrettyPrint
  ( module Text.PrettyPrint.Leijen
  , render
  , renderPretty
  , renderCompact
  , ($$)
  , ($$>)
  , (//)
  , (//>)
  , defaultShow
  , condParens
  , parens
  , Pretty(..)
  , list
  , separated
  , prettyApp
  , hang
  , indent
  , tupled
  , Verbose(..)
  , (…)
  , Error'(..), Error
  ) where

import qualified Data.Map.Strict                  as Map.Strict
import qualified Data.Set                         as Set
import qualified Data.List.NonEmpty               as NE
import qualified Text.PrettyPrint.Leijen          as PP
import           Text.PrettyPrint.Leijen          hiding ((<$>), (<$$>), renderPretty, renderCompact, Pretty(..), list, parens, tupled, hang, indent, (<>))

import           Tog.Prelude

render :: Pretty a => a -> String
render x = defaultShow 0 x ""

renderPretty :: Pretty a => Int -> a -> String
renderPretty width_ x =
  displayS (PP.renderPretty 0.7 width_ (pretty x)) ""

renderCompact :: Pretty a => a -> String
renderCompact x = displayS (PP.renderCompact (pretty x)) ""

infixr 5 $$
infixr 5 $$>

infixr 6 //
infixr 6 //>

list :: [Doc] -> PP.Doc
list xs0 = PP.group $ case xs0 of
  []       -> "[]"
  (x : xs) -> nest 2 ("[" <+> x) <> PP.line <> go xs
  where
    go []       = "]"
    go (x : xs) = nest 2 ("," <+> x) <> PP.line <> go xs

separated :: Doc -> [Doc] -> PP.Doc
separated sep xs0 = PP.group $ case xs0 of
  []       -> ""
  (x : xs) -> nest 2 (x) <> PP.line <> go xs
  where
    go []       = ""
    go (x : xs) = nest 2 (sep <+> x) <> PP.line <> go xs

tupled :: [Doc] -> Doc
tupled = encloseSep lparen rparen (comma <> space)

-- | Hard break
($$) :: Doc -> Doc -> Doc
x $$ y = x <> line <> y

-- | Hard break with indent
($$>) :: Doc -> Doc -> Doc
x $$> y = x <> nest 2 (line <> y)

-- | Soft break
(//) :: Doc -> Doc -> Doc
x // y = x <> group (line <> y)

-- | Soft break with indent
(//>) :: Doc -> Doc -> Doc
x //> y = x <> group (nest 2 (line <> y))

defaultShow :: Pretty a => Int -> a -> ShowS
defaultShow p = displayS . PP.renderPretty 0.7 80 . prettyPrec p

condParens :: Bool -> Doc -> Doc
condParens True  = parens
condParens False = id

parens :: Doc -> Doc
parens x = char '(' <> align x <> char ')'

instance IsString Doc where
  fromString = text

class Pretty a where
  {-# MINIMAL pretty | prettyPrec #-}

  pretty :: a -> Doc
  pretty = prettyPrec 0

  prettyList :: [a] -> Doc
  prettyList  = encloseSep lbracket rbracket (comma <> space) . map pretty

  prettyPrec :: Int -> a -> Doc
  prettyPrec _ = pretty

instance Pretty a => Pretty [a] where
  pretty        = prettyList

instance Pretty Doc where
  pretty        = id

instance Pretty () where
  pretty ()     = text "()"

instance Pretty Char where
  pretty c      = char c
  prettyList s  = string s

instance Pretty Int where
  pretty i      = int i

instance Pretty Integer where
  pretty i      = integer i

instance Pretty Float where
  pretty f      = float f

instance Pretty Double where
  pretty d      = double d

instance Pretty Bool where
  pretty True   = "yes"
  pretty False  = "no"

instance (Pretty a,Pretty b) => Pretty (a,b) where
  pretty (x,y)  = tupled [pretty x, pretty y]

instance (Pretty a,Pretty b,Pretty c) => Pretty (a,b,c) where
  pretty (x,y,z)= tupled [pretty x, pretty y, pretty z]

instance Pretty a => Pretty (Maybe a) where
  pretty Nothing        = empty
  pretty (Just x)       = pretty x

instance Pretty Natural where
  pretty = text . show

instance (Pretty k, Pretty v) => Pretty (Map.Strict.Map k v) where
  pretty m = "Map.Strict.fromList" <+> pretty (Map.Strict.toList m)

instance (Pretty v) => Pretty (Set.Set v) where
  pretty s = "Set.fromList" <+> pretty (Set.toList s)

instance (Pretty v) => Pretty (NE.NonEmpty v) where
  pretty = pretty . NE.toList

prettyApp :: Pretty a => Int -> Doc -> [a] -> Doc
prettyApp _ h []   = h
prettyApp p h args0 = condParens (p > 3) $ h <> nest 2 (group (prettyArgs (reverse args0)))
  where
    prettyArgs []           = empty
    prettyArgs [arg]        = line <> prettyPrec 4 arg
    prettyArgs (arg : args) = group (prettyArgs args) $$ prettyPrec 4 arg

hang :: Natural -> Doc -> Doc
hang n d = PP.hang (fromIntegral n) d

indent :: Natural -> Doc -> Doc
indent n d = PP.indent (fromIntegral n) d

newtype Verbose a = Verbose a

data (:…) = (:…) deriving (Show)

(…) :: (:…)
(…) = (:…)

instance Pretty (:…) where
  pretty (:…) = "…"

instance (Pretty a, Pretty b) => Pretty (a :∈ b) where
  pretty (a :∈ b) = pretty a <+> "∈" <+> pretty b

-- | Type for errors. Includes a description of the error and an error code
data Error' d = Error {
    errCode :: Int
  , errDoc  :: d
  } deriving (Functor, Foldable, Traversable)

type Error = Error' PP.Doc

instance Pretty a => Pretty (Error' a) where             
  pretty = pretty . errDoc

instance Pretty a => Pretty (Tagged k a) where pretty = pretty . unTagged
