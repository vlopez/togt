module Tog.Term.Synonyms where

-- Useful type synonyms
------------------------------------------------------------------------

type Type t = t
type Term t = t

type Closed t = t
