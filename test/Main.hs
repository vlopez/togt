module Main where

import Tog.Test.Utils
import qualified Tog.Test.Suite as Suite
import Tog.Test.Suite.Utils
import Tog.Test.Unit
import Tog.Test.Refinement
import Tog.Test.Utils.RunHaskell

import Test.Tasty
-- Issue: Can't use the Interactive version of Tasty.Silver because it is not
-- compatible with expected failure.
-- import qualified Test.Tasty.Silver.Interactive as Interactive
import Control.Lens ((<&>))

import qualified Paths_tog
import System.FilePath ((</>))

import Data.List (intercalate)
import qualified Data.Text.IO as T

testCaseDir :: String
testCaseDir = "test/cases"

togExec :: IO RunTog
togExec = do
  path <- Paths_tog.getBinDir <&> (</> "tog") 
  return RunTog{
     _execPath = path
    ,_execOpts = mempty
    ,_rtsOpts  = dd "M" "1G"
    ,_timeLimit = Unlimited  -- ^ time limits are determined by the test suite
    }

fileTests :: IO [TestTree]
fileTests = do
  exec  <- togExec
  cases <- getTestCases testCaseDir
  return$ Suite.allTests exec cases

main :: IO ()  
main = do
  putStrLn ""
  putStrLn =<< intercalate " " . runTogCommandLine <$> togExec 
  T.putStrLn =<< togVersion =<< togExec

  defaultMain =<< testGroup "tog" <$> sequence [
      testGroup "file"   <$>       fileTests 
    , testGroup "unit"   <$>  pure unitTests
    , testGroup "refinement" <$>  pure refinementTests
    ]
