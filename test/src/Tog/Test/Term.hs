{-# LANGUAGE PartialTypeSignatures #-}
{-# LANGUAGE NoImplicitPrelude #-}
module Tog.Test.Term (
    mkEq
  , module Tog.Monad
  , module Tog.Term
  , module Tog.Names.Sorts
  -- * Running the unifier
  , UnifierProg
  , UnifierSetup(..), {-setupHeterogenous, setupSimple,-} setupTwin
  , UnifierRun(..)
  , runUnifier
  -- * Term building
  , qname, qname_, v, (-->), (*-->), lamM, (*$), mkClause, inC0
  -- * Error checking
  )

where

import  Tog.Prelude

import  qualified Tog.Term.Impl as TI
import  qualified Tog.Unify.Impl as UI
import  Tog.Term
import  Tog.Names
import  Tog.Names.Sorts
import  Tog.Monad
import  qualified Tog.Elaborate as Elaborate
import  qualified Tog.PrettyPrint as PP

import  Tog.Unify.Class

import  Tog.Instrumentation.Conf (runConfT, Conf, defaultConf)

data UnifierSetup where UnifierSetup :: forall h t. (forall r. (Dict (Unifier h t) -> r) -> r) -> UnifierSetup

{-setupSimple, setupHeterogenous, -}
setupTwin :: UnifierSetup
-- setupSimple =       UnifierSetup @UI.Simple        @TI.Simple (\r -> r Dict)
-- setupHeterogenous = UnifierSetup @UI.Heterogeneous @TI.Simple (\r -> r Dict)
setupTwin =  UnifierSetup @UI.Twin @TI.Simple (\r -> r Dict)

type UnifierProg = forall h t. Unifier h t => UF h t ()

-- Building terms and constraints
qname_ :: String -> QName
qname_ lbl = qName (mkName lbl) []

-- Building terms and constraints
qname :: forall sort. String -> QNameS sort
qname = sTrustMe . qname_

sequenceCtx :: MonadNames m => Ctx (m t) -> m (Ctx t)
sequenceCtx C0 = pure C0
sequenceCtx (ctx :< (name, t)) = (:<) <$> sequenceCtx ctx <*> ((name,) <$> localCtx ctx t)

mkEq :: Monad m => Ctx (NamesT m t) -> (NamesT m (Term t) :∈ NamesT m (Type t)) -> (NamesT m (Term t) :∈ NamesT m (Type t)) ->
        m (Elaborate.Constraint t)
mkEq ctx (t1 :∈ ty1) (t2 :∈ ty2) = do
  Elaborate.JmEq noSrcLoc
    <$> runNamesT C0 (sequenceCtx ctx)
    <*> ((:∋) <$> runNamesT ctx ty1
              <*> runNamesT ctx t1)
    <*> ((:∋) <$> runNamesT ctx ty2
              <*> runNamesT ctx t2)

v :: (MonadTerm t m, MonadNames m) => Name -> m t
v name = lookupCtxName name >>= var

infixr 9 -->, *-->
(-->) :: (MonadNames m, MonadTerm t m) => (Name, m t) -> m t -> m t
(name, mdom) --> mcod = do
  dom <- mdom
  cod <- localAddName name $ mcod
  pi dom (Abs name cod)

(*-->) :: (MonadNames m, MonadTerm t m) => m t -> m t -> m t
type_ *--> t = ("_",type_) --> t

lamM :: (MonadNames m, MonadTerm t m) => Name -> m t -> m t
lamM name m = lam . Abs name =<< localAddName name m

inC0 :: Monad m => NamesT m a -> m a
inC0 = runNamesT C0

(*$) :: MonadTerm t m => m (Term t) -> m (Term t) -> m (Term t)
f *$ x = join$ eliminate <$> f <*> sequence [Apply <$> x]
infixl 5 *$

data MkPattern t =
    MkVarP Name
  | MkConP (Opened DConName t) [MkPattern t]
  | MkEmptyP

instance IsString (MkPattern t) where
  fromString = MkVarP . fromString

mkClause :: Monad m => [MkPattern t] -> NamesT m (ClauseBody t) -> m (Clause t)
mkClause ps t = do
  let (names, patterns) = traverse getPattern ps
  body <- runNamesT C0 $ localAddNames names t
  return$ Clause patterns body 

  where
    getPattern :: MkPattern t -> ([Name], Pattern t)
    getPattern (MkVarP name)  = tell [name] >> pure VarP
    getPattern (MkConP op ps) = ConP op <$> traverse getPattern ps 
    getPattern (MkEmptyP) = pure EmptyP


data UnifierRun = UnifierRun {
    prog     :: UnifierProg
  , conf     :: Conf
  , setup    :: UnifierSetup
  }

runUnifier :: UnifierRun -> IO (Either PP.Error ())
runUnifier UnifierRun{prog,conf,setup=UnifierSetup dict} = dict $ \d@Dict -> f d prog
  where
    f :: forall h t. Dict (Unifier h t) -> UF_ h t () () -> IO _
    f Dict m = do
      (err,_,_) <- flip runConfT conf$ do
            sig <- sigEmpty
            runTC
              sig
              ()
              (initSolveState @h @t)
              m
      return err
