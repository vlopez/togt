{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedLists #-}
{-# LANGUAGE PartialTypeSignatures #-}
{-# LANGUAGE DataKinds #-}
{-# OPTIONS -fno-warn-partial-type-signatures #-}
module Tog.Test.Unit (unitTests) where

import Tog.Prelude

import Tog.Test.Unit.Utils
import Tog.Test.Term
import Test.Tasty (TestTree)
import Test.Tasty.ExpectedFailure (ignoreTest)

import Tog.Monad
import Data.List (isPrefixOf)

data StandardSig t = StandardSig {
   tA :: Type t
  ,tB :: Type t
  ,ta :: Term t
  ,tb :: Term t
  ,tconst :: Term t
  ,tid :: Term t
  ,tab :: Term t
  ,ttAxtA :: Type t
  ,pfst :: Elim t
  ,psnd :: Elim t
  ,pair :: Opened RConName t
  }

getStandardSig :: IsTerm t => TC t r s (StandardSig t)
getStandardSig = do
  addPostulate (qname "A") T0 set; tA <- def (Opened (qname @'PDefS "A") []) []
  addPostulate (qname "B") T0 set; tB <- def (Opened (qname @'PDefS "B") []) []

  -- a : A
  addPostulate (qname "a") T0 =<< (inC0$ pure tA); ta <- def (Opened (qname @'PDefS "a") []) []

  -- b : B
  addPostulate (qname "b") T0 =<< (inC0$ pure tB); tb <- def (Opened (qname @'PDefS "b") []) []

  -- ab : (A : Set) (B : Set) -> Set
  addPostulate (qname "ab") T0 =<< (inC0$ pure tA *--> pure tB *--> pure set); tab <- def (Opened (qname @'PDefS "ab") []) []

  -- id : (A : Set) -> A -> A
  addTypeSig   (qname @'FDefS "id") T0 =<< (inC0$ ("A", pure set) --> (v "A" *--> v "A"));
  tid <- def (Opened (qname @'FDefS "id") []) []
  addClauses (Opened (qname @'FDefS "id") []) =<< (NotInvertible <$> sequence [mkClause ["_","x"] (ClauseBody <$> v "x")])

  -- const : (A : Set) (B : Set) -> A -> B -> A
  addTypeSig   (qname @'FDefS "const") T0 =<< (inC0$ ("A", pure set) --> ("B", pure set) --> (v "A" *--> v "B" *--> v "A"));
  tconst <- def (Opened (qname @'FDefS "const") []) []
  addClauses (Opened (qname "const") []) =<< (NotInvertible <$> sequence [mkClause ["_","_","x","y"] (ClauseBody <$> v "x")])

  -- pair
  addTypeSig (qname @'TyConS "tA×tA") T0 =<< (inC0$ pure set); let tyTimes = Opened (qname @'TyConS "tA×tA") []
  ttAxtA <- def tyTimes []
  pair <- pure$ Opened (qname "pair") []; addRecordCon tyTimes (qname "pair")
  -- fst
  pFst <- pure$ Projection' (qname "fst") (Field 0); pfst <- pure$ Proj pFst
  addProjection pFst tyTimes =<< (Contextual T0 <$> (inC0$ pure ttAxtA *--> pure tA))
  -- snd
  pSnd <- pure$ Projection' (qname "snd") (Field 1); psnd <- pure$ Proj pSnd
  addProjection pSnd tyTimes =<< (Contextual T0 <$> (inC0$ pure ttAxtA *--> pure tA))
  -- close off pair
  addRecordConSignature (qname "pair") tyTimes 2 =<< (Contextual T0 <$> (inC0$ pure tA *--> pure tA *--> pure ttAxtA))

  return StandardSig{..}

allSetups :: UnifierSetup
allSetups = setupTwin

failureExpected :: String -> String -> Bool 
failureExpected name conf =  "W;" `isPrefixOf` conf && name `elem` ([] :: [_])

unitTests :: [TestTree]
unitTests = [
  mk "no-metas" UnifierRun{
        prog = do
          StandardSig{tA,tB,tconst,tab} <- getStandardSig
          solve =<< sequence
            [mkEq (C0 :< ("x", pure tA) :< ("y", pure tB))
              ((pure tab *$ (pure tconst *$ pure tA *$ pure tB *$ v "x" *$ v "y") *$ v "y") :∈ pure set)
              ((pure tab *$ v "x" *$ (pure tconst *$ pure tB *$ pure tA *$ v "y" *$ v "x")) :∈ pure set)
            ]
          noUnsolvedProblems
      , conf   = defaultTestConf
      , setup  = allSetups 
      }
  ,

  mk "no-metas-fail" UnifierRun{
        prog = do
          StandardSig{tA,tconst,tab} <- getStandardSig
          expectTypeCheckerError $ solve =<< sequence
            [mkEq (C0 :< ("x", pure tA) :< ("y", pure tA))
              ((pure tab *$ (pure tconst *$ pure tA *$ pure tA *$ v "x" *$ v "y") *$ v "x") :∈ pure set)
              ((pure tab *$ v "x" *$ (pure tconst *$ pure tA *$ pure tA *$ v "y" *$ v "x")) :∈ pure set)
            ]

      , conf   = defaultTestConf
      , setup  = allSetups 
      }
  ,
  mk "fst" UnifierRun{
        prog  = do
            StandardSig{tA} <- getStandardSig

            -- m1 : A -> A -> A
            m1 <- addMeta =<< (runNamesT C0$ pure tA *--> pure tA *--> pure tA)

            solve =<< sequence
                  [mkEq (C0 :< ("x", pure tA) :< ("y", pure tA))
                          ((meta m1 =<< sequence [Apply <$> v "x"
                                                 ,Apply <$> v "y"
                                                 ])
                            :∈ pure tA)
                          (v "x" :∈ pure tA)
                  ]

            noUnsolvedProblems
            metaEquals m1 =<< runNamesT C0 (lamM "x" (lamM "y" (v "x")))

      , conf   = defaultTestConf
      , setup  = allSetups
      }
  ,

  mk "non-linear" UnifierRun{
        prog  = do
            StandardSig{tA} <- getStandardSig

            -- m1 : A -> A -> A
            m1 <- addMeta =<< (runNamesT C0$ pure tA *--> pure tA *--> pure tA)

            solve =<< sequence
                  [mkEq (C0 :< ("x", pure tA) :< ("y", pure tA))
                          ((meta m1 =<< sequence [ Apply <$> v "x"
                                                 , Apply <$> v "x"
                                                 ])
                            :∈ pure tA)
                          (v "x" :∈ pure tA)
                  ]

            someUnsolvedProblems
            metaIsUninstantiated m1

      , conf   = defaultTestConf
      , setup  = allSetups
      }
  ,
    mk "def-eta" UnifierRun{
        prog  = do
            StandardSig{tA} <- getStandardSig

            [tf,tg] <- forM (["f","g"] :: [String]) $ \f -> do
            -- f, g : A -> A
              addTypeSig  (qname @'FDefS f) T0 =<< (inC0$ pure tA *--> pure tA)
              tf <- def (Opened (qname @'FDefS f) []) []
              addClauses (Opened (qname @'FDefS f) []) =<< (NotInvertible <$> sequence [mkClause ["x"] (ClauseBody <$> v "x")])
              return tf

            solve =<< sequence
                  [mkEq C0
                          (pure tf :∈ (pure tA *--> pure tA))
                          (pure tg :∈ (pure tA *--> pure tA))
                  ]

            noUnsolvedProblems

      , conf   = defaultTestConf
      , setup  = allSetups
      }
  ,
    ignoreTest$ mk "curry-when-pruning" UnifierRun{
        prog  = do
            StandardSig{tA,pair,ttAxtA,pfst} <- getStandardSig

            addPostulate (qname "foo") T0 =<< (inC0$ pure tA *--> pure set); let foo = (Opened (qname @'PDefS "foo") [])
            mX  <- addMeta =<< (inC0$ pure tA *--> pure set)
            mY  <- addMeta =<< (inC0$ pure ttAxtA *--> pure tA)

            solve =<< sequence
                  [mkEq (C0 :< ("x", pure tA) :< ("y", pure tA))
                          ((meta mX =<< sequence [ Apply <$> v "x" ]) :∈ pure set)
                          ((def foo =<< sequence [ Apply <$> (meta mY =<< sequence [ Apply <$> (con pair =<< sequence [ v "x", v "y" ]) ]) ]) :∈ pure set)
                  ,mkEq (C0 :< ("x", pure tA)) 
                          ((meta mY =<< sequence [Apply <$> (con pair =<< sequence [ v "x", v "x" ])]) :∈ pure tA)
                          (v "x" :∈ pure tA)
                  ]

            noUnsolvedProblems
            metaEquals mX =<< runNamesT C0 (lamM "x" (def foo =<< sequence [ Apply <$> v "x" ]))
            metaEquals mY =<< runNamesT C0 (lamM "x" (v "x" >>= (`eliminate` [ pfst ])))
            return ()

      , conf   = defaultTestConf
      , setup  = allSetups
      }
    ,
    mk "intersection" UnifierRun{ 
        prog  = do
            StandardSig{tA} <- getStandardSig

            mY  <- addMeta =<< (inC0$ pure tA *--> pure tA *--> pure tA)

            solve =<< sequence
                  [mkEq (C0 :< ("x", pure tA) :< ("y", pure tA))
                          ((meta mY =<< sequence (fmap Apply <$> [ v "x", v "x" ])) :∈ pure tA)
                          ((meta mY =<< sequence (fmap Apply <$> [ v "y", v "x" ])) :∈ pure tA)
                   
                  ,mkEq (C0 :< ("x", pure tA)) 
                          ((meta mY =<< sequence (fmap Apply <$> [ v "x", v "x" ])) :∈ pure tA)
                          (v "x" :∈ pure tA)
                  ]

            noUnsolvedProblems
            metaEquals mY =<< runNamesT C0 (lamM "x" (lamM "y" (v "y")))
            return ()

      , conf   = defaultTestConf
      , setup  = allSetups
      }
    ,
    mk "equal-meta-head" UnifierRun{
        -- Correct behaviour: intersect the vars
        -- Incorrect behaviour: fail the occurs check for lack of normalization
        prog = do
            StandardSig{tA,tid} <- getStandardSig

            mY <- addMeta =<< (inC0$ pure tA *--> pure tA *--> pure tA)

            solve =<< sequence
                  [mkEq (C0 :< ("x", pure tA) :< ("y", pure tA))
                          ((meta mY =<< sequence (fmap Apply <$> [ v "x", v "y" ])) :∈ pure tA)
                          ((eliminate tid =<< sequence [pure$ Apply tA, Apply <$> (meta mY =<< sequence (fmap Apply <$> [ v "y", v "y" ]))]) :∈ pure tA)
                   
                  ,mkEq (C0 :< ("x", pure tA)) 
                          ((meta mY =<< sequence (fmap Apply <$> [ v "x", v "x" ])) :∈ pure tA)
                          (v "x" :∈ pure tA)
                  ]

            noUnsolvedProblems
            metaEquals mY =<< runNamesT C0 (lamM "x" (lamM "y" (v "y")))
            return ()
      , conf   = defaultTestConf{confNfInUnifier=True}
      , setup  = allSetups
      }      
  ]
  where mk = mkUnitTest failureExpected

