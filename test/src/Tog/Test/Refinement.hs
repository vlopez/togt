{-# LANGUAGE GeneralizedNewtypeDeriving #-}
module Tog.Test.Refinement where

import Test.Tasty
import Test.Tasty.QuickCheck

import Data.Refinement (Refinement, empty, branch, isSolved, areAllSolved, hasChildren)

import qualified Data.ISet      as IS
import Data.List (nub)

import Control.Monad.State

-- Universe of elements for the tests
newtype Atom = Atom Int deriving (Eq, Show, Ord, Enum)
instance IS.ISetMember Atom where repr (Atom i) = i 

newtype Universe = Universe [Atom] deriving (Eq, Show)

instance Arbitrary Atom where
  arbitrary = Atom <$> arbitrarySizedNatural 

instance Arbitrary Universe where
  arbitrary = Universe . nub <$> listOf1 arbitrary

subUniverse :: Universe -> Gen [Atom]
subUniverse (Universe us) = sublistOf us

oneAtom :: Universe -> Gen Atom
oneAtom (Universe us) = elements us

treeInU :: Universe -> Gen (Refinement Atom)
treeInU (Universe us) =
  case us of
    (parent:children) -> do
      solved <- sublistOf children
      return$ flip execState empty $ do
        modify$ branch parent children 
        forM_ solved $ \c -> modify (branch c [])
    _ -> discard

newUniverse :: Universe -> Gen Universe
newUniverse (Universe us0) = do
  Universe us <- arbitrary
  let us' = [u | u <- us, not$ u `elem` us0]
  if length us' > 0 then pure (Universe us') else discard
    
refinementTests :: [TestTree]
refinementTests = [
   -- Empty tree has no solved nodes
   testProperty "empty" $ \(a :: Atom) ->
     isSolved a empty === False 
   -- All solved corresponds exactly to (all . map isSolved)

  ,testProperty "solved-all-solved" $
     \us ->
        forAll (subUniverse us) $ \us0 -> 
          forAll (treeInU us) $ \tree ->
            areAllSolved (IS.fromList us0) tree === all (flip isSolved tree) us0

   -- Branching to empty makes the node solved
  ,testProperty "branch-empty" $      \us ->
       forAll (treeInU us) $ \tree ->
         forAll (oneAtom us) $ \atom ->                                 
           not (isSolved atom tree) && not (hasChildren atom tree) ==>
               isSolved atom (branch atom [] tree)

  ,testProperty "no-children-if-solved" $ \us ->
       forAll (treeInU us) $ \tree ->
         forAll (oneAtom us) $ \atom ->                                 
           not (isSolved atom tree && hasChildren atom tree)

   -- Branching to non-empty makes the parent solved
  ,testProperty "branch-non-empty-parent" $ \us ->
      forAll (treeInU us) $ \tree ->
         forAll (oneAtom us) $ \parent ->                                 
           not (isSolved parent tree) && not (hasChildren parent tree) ==>
             forAll (newUniverse us) $ \(Universe children) ->
               forAll (pure$ branch parent children tree) $ \treeB ->
                 length children > 0 ==> not (isSolved parent treeB)

  ,testProperty "branch-one-children-parent" $ \us ->
      forAll (treeInU us) $ \tree ->
         forAll (oneAtom us) $ \parent ->                                 
           not (isSolved parent tree) && not (hasChildren parent tree) ==>
             forAll (newUniverse us) $ \(Universe children) ->
               forAll (pure$ branch parent children tree) $ \treeB ->
                 forAll (pure$ branch (head children) [] treeB) $ \treeC ->
                   length children > 1 ==> not (isSolved parent treeC)
                   
  ,testProperty "branch-all-children-parent" $ \us ->
      forAll (treeInU us) $ \tree ->
         forAll (oneAtom us) $ \parent ->                                 
           not (isSolved parent tree) && not (hasChildren parent tree) ==>
             forAll (newUniverse us) $ \(Universe children) ->
               forAll (pure$ branch parent children tree) $ \treeB ->
                 forAll (pure$ flip execState treeB $ do
                            forM_ children $ \c -> modify (branch c [])) $ \treeC ->
                   isSolved parent treeC

   -- Create a long chain of children
  ,testProperty "chain" $ \(Universe us) ->
      let end = last us in
      forAll (pure$ flip execState empty $ do
            forM (zip (init us) (tail us)) $ \(p,c) -> modify (branch p [c])) $ \tree ->
        (all (not . flip isSolved tree) us) .&&.
          (all (flip isSolved (branch end [] tree)) us)

   -- Create a long chain of children
  ,testProperty "chain-2-not-solved" $ \(Universe us) ->
      let children = zip (tail us) (tail [foldl1 max us..])
          allChildren = map fst children ++ map snd children
      in
      length allChildren > 0 ==>
        forAll (pure$ flip execState empty $ do
              forM (zip (init us) children) $ \(p,(c1,c2)) -> modify (branch p [c1,c2])) $ \tree ->
          forAll (elements (last (map fst children):map snd children)) $ \drop ->
            all (not . flip isSolved tree) (us ++ allChildren) .&&. (
                let tree' = branch drop [] tree in
                all (not . flip isSolved tree') (init us) .&&.
                   isSolved drop tree'
                )
  ]


