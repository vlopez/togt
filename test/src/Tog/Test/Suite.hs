module Tog.Test.Suite (allTests) where

import Tog.Error (noErrorCode, isOffsetedError)

import Test.Tasty (TestTree)
import Test.Tasty.HUnit
import Test.Tasty.Silver
import Test.Tasty.ExpectedFailure (expectFail, ignoreTest)

import Test.Tasty
import Tog.Test.Utils
import Tog.Test.Utils.RunHaskell
import Tog.Test.Suite.Utils
import Tog.Test.Config

import Tog.Test.Files.Dirs

import Data.List
import qualified Data.Text as T
import Control.Applicative

import Control.Lens

exhaustive :: [Configuration -> Configuration]
exhaustive = [id, (<> and_noSynEq)]

and_HC4P_noCheck :: Configuration
and_HC4P_noCheck = (and_HC4P <> and_noCheck <> and_noSynEq)

configurations :: TestCaseFamily -> [Configuration]
configurations SucceedAll           = {-([with_T, with_S] <**> [id, (<> and_HC3), (<> and_HC4), (<> and_P), (<> and_PH), (<> and_HC4P_noCheck)] <**> exhaustive)
                                      ++-} ([with_W]  <**> [id, (<> and_P), (<> and_HC4)] <**> [id, (<> and_FE)])

configurations SucceedHeterogeneous = {-([with_T] <**> [(<> and_HC3), (<> and_HC4), (<> and_P), (<> and_PH), (<> and_HC4P_noCheck)] <**> exhaustive)
                                      ++-} ([with_W]  <**> [id, (<> and_P), (<> and_HC4)] <**> [id, (<> and_FE)])

configurations Fail                 = {-([with_T] ++ [with_S] <**> [id, (<> and_P), (<> and_PH), (<> and_HC4P_noCheck)])
                                      ++-} ([with_W] <**> [id, (<> and_P), (<> and_HC4)]  <**> [id, (<> and_FE)])

configurations SlowAll              = {-([with_S, with_T] <**> [(<> and_HC3), (<> and_HC4)] <**> [(<> and_noCheck)])
                                      ++-} [with_W <> and_HC4 <> and_noCheck] <**> [id, (<> and_FE)]

configurations SlowHeterogeneous    = {-(([with_T] <**> [(<> and_HC3), (<> and_HC4)]) <&> (<> and_noCheck))
                                      ++-} [with_W <> and_HC4 <> and_noCheck] <**> [id, (<> and_FE)]

configurations Constraints          = {-([with_S, with_T] <**> [id, (<> and_HC3), (<> and_HC4)] <**> exhaustive)
                                      ++-} ([with_W]  <**> [id, (<> and_P), (<> and_HC4)]) <**> [id, (<> and_FE)]

configurations Benchmark            = {-([with_S, with_T] <**> [(<> and_HC3), (<> and_HC4)] <**> [(<> and_fastLoose)] <**> [id, (<> and_Nf)])
                                      ++-}   ([with_W <> and_HC4 <> and_fastLoose] <**> [id,(<> and_Nf)]) <**> [id, (<> and_FE)]

configurations BenchmarkHeterogeneous = {-([with_T] <**> [(<> and_HC3), (<> and_HC4)] <**> [(<> and_fastLoose)] <**> [id, (<> and_Nf)])
                                        ++-} ([with_W <> and_HC4 <> and_fastLoose] <**> [id,(<> and_Nf)]) <**> [id, (<> and_FE)]

allFamilies :: [TestCaseFamily]
allFamilies = enumAll

allConfigs :: [Configuration]
allConfigs = nub$ sort$ [c | fam <- allFamilies, c <- configurations fam]

allTests :: RunTog -> (TestCaseFamily -> [TestCase]) -> [TestTree]
allTests run cases =
    [ testGroup (configName cfg) [
        testGroup (testDirName fam) [
          withFixedConfig t (config cfg) $ testTog run
        | t <- cases fam
        ]
      | fam <- allFamilies
      , cfg `elem` nub (configurations fam)
      ]
    | cfg <- allConfigs]


assertTogSuccess :: TogResult -> Assertion
assertTogFailure :: TogResult -> IO Int

assertTogSuccess Valid = return ()
assertTogSuccess TimedOut = assertFailure "Timed out"
assertTogSuccess res@(NotValid{}) = assertFailure$ T.unpack$ mkGoldenFile res

assertTogFailure Valid = fail "Did not fail"
assertTogFailure TimedOut = fail "Timed out"
assertTogFailure res@(NotValid code _doc) | code == noErrorCode
                                             || not (isOffsetedError code) = fail$ T.unpack$ mkGoldenFile res
                                          | otherwise = return code

redefineSuccess :: TestCase -> TogConf -> (TestTree -> TestTree)
redefineSuccess (((,) <$> testCaseFamily <*> testCaseName) -> (family, name)) conf
  | testIgnored     family name conf = ignoreTest
  | failureExpected family name conf = expectFail
  | otherwise                     = id

withFixedConfig :: TestCase -> TogConf -> (TestCase -> TogConf -> TestTree) -> TestTree
withFixedConfig t cfg κ = do
  let name = testCaseName t
  if name `elem` [--"J"
                 --,"Vec"
                 --,"Data_mini"
                 --"Language_test5"
                 --,"Language_test5_mini"
                 --,"Language_test5_mini_1"
                 --,"Language_test5_mini_lookup"
                 -- "SigmaPostponed"
                 --,"PredicateArgument"

                 -- Keep it here because otherwise 'MA' does not normalize enough for intersection to take place
                  "Intersection"

                 -- Keep it here because otherwise 'Y' does not normalize enough for the pruning to take place
                 ,"CurryWhenPruning"

                 -- Keep it here because otherwise it is too slow
                 -- ,"Data"

                 ]
    then
      testGroup "nf" [κ t (config$ Configuration "" cfg <> and_Nf)]
    else
      κ t cfg
 
failureExpected, testIgnored :: String -> String -> DashDash -> Bool                                         
failureExpected _family name conf = (name,conf) `elem` [
--                                 ("Basic",                       config$ with_T <> and_HC4P_noCheck)
--                               , ("J",                           config$ with_T <> and_HC4P_noCheck)
--                               , ("Language_test1",              config$ with_T <> and_HC4P_noCheck)
--                               , ("Language_test2",              config$ with_T <> and_HC4P_noCheck)
                              ] ||
                            (conf `getParam` "solver" `elem` [Just "S", Just "T"] &&
                              "Language_test5" `isPrefixOf` name)
                                ||
                            (conf `getParam` "solver" `elem` [Just "T"] &&
                              "DefEta2" == name)
                                ||
                            (conf `getParam` "solver" ==  Just "W"
                               &&       (name  `elem`
                                              [] ++
                                              [] ++
                                              [])
                            )

testIgnored     _family name conf = (name, conf) `elem` [
                                  ("Basic",                       config$ with_T <> and_PH <> and_noSynEq)
                                , ("Basic_mini",                  config$ with_T <> and_PH <> and_noSynEq)
                                , ("J",                           config$ with_T <> and_PH <> and_noSynEq)

                                , ("Data_mini",                   config$ with_S <> and_HC4P_noCheck)
                                , ("HierReduced_mini",            config$ with_S <> and_HC4P_noCheck)
                                , ("Language_test2",              config$ with_S <> and_HC4P_noCheck)
                                , ("Vec",                         config$ with_S <> and_HC4P_noCheck)
   
                                , ("Data_mini",                   config$ with_T <> and_HC4P_noCheck)
                                , ("Vec",                         config$ with_T <> and_HC4P_noCheck)
                                , ("HierReduced_mini",            config$ with_T <> and_HC4P_noCheck)
                                , ("Language_test5_mini",         config$ with_T <> and_HC4P_noCheck)
                                , ("Language_test5_mini_1",       config$ with_T <> and_HC4P_noCheck)
                                , ("Language_test_mini_lookup",   config$ with_T <> and_HC4P_noCheck)

                                , ("Nested_mini",                 config$ with_T <> and_PH <> and_noSynEq)

                                -- too slow for some reason
                                , ("Language_test4",              config$ with_W <> and_FE)

                             ] || (getParam conf "termType" == Just "PH" &&
                                   name `elem` ["Data_mini","HierReduced_mini","Vec"
                                               ,"Language_test5_mini","Language_test5_mini",
                                                                      "Language_test5_mini_1",
                                                                      "Language_test_mini_lookup"])

                               -- Too slow, does not seem type correct
                               || (name `elem` ["Language_test7"])
                               -- Too slow, working on it
                               -- || (name `elem` ["Language_test5"])
                               -- || (name `elem` ["Language_test6"])
                               || (name `elem` ["CurryWhenPruning"])
                               

testTog :: RunTog -> TestCase -> TogConf -> TestTree
testTog run t conf =
  redefineSuccess t conf $ case t of
    SuccessCase{theFile} ->
      testCase (testCaseName t) (runTog (run & execOpts <>~ ddPos theFile <> conf) >>=
                             passthrough perhapsLogTogResult >>=
                             assertTogSuccess)
    FailureCase{theFile,goldenFile} ->
      goldenVsAction
          (testCaseName t)
          (goldenFile)
          (runTog (run & execOpts <>~ ddPos theFile <> conf) >>=
             passthrough perhapsLogTogResult >>= assertTogFailure)
          (\code -> T.pack$ "ExitFailure " <> show code)

