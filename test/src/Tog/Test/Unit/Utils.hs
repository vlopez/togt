{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE AllowAmbiguousTypes #-}
module Tog.Test.Unit.Utils (
    mkUnitTest
  , defaultTestConf
  , UnitTest(..)
  , solve
  , noUnsolvedProblems
  , someUnsolvedProblems
  , metaIsUninstantiated
  , metaEquals
  , expectTypeCheckerError
  -- * Extras
  , Conf(..) 
  )
where

import  Tog.Prelude
import  Tog.Instrumentation.Conf (runConfT, Conf(..), defaultConf)
import  Test.Tasty.HUnit (testCase, assertFailure, Assertion)
import  Test.Tasty (testGroup, TestTree)
import  Control.Monad.IO.Class (MonadIO(..))

import  Tog.Unify.Class

import qualified Tog.PrettyPrint as PP
import Tog.PrettyPrint ((<+>))
import Data.Text (Text)
import qualified Data.Text as T

import Test.Tasty.ExpectedFailure (expectFail)
import Control.Exception (SomeException, catch, displayException) 

import Tog.Test.Term

defaultTestConf :: Conf
defaultTestConf = defaultConf{
       confCheckMetaConsistency = True
      ,confUnifyAudit = True
      }

mkUnifierTest :: forall h t. Unifier h t => String
           -> Conf
           -> UF_ h t () () -> TestTree
mkUnifierTest lbl conf m = testCase lbl run
  where
    run :: Assertion
    run =
      assertNoExceptions (flip runConfT conf$ do
            sig <- sigEmpty
            runTC
              sig
              ()
              (initSolveState @h @t)
              m) $
        \(err, _sig, _res) ->
          case err of
            Left doc -> failTest (PP.pretty doc)
            Right () -> return ()

assertNoExceptions :: IO a -> (a -> Assertion) -> Assertion
assertNoExceptions m κ = do
  catch @SomeException (Right <$> m) (pure . Left . displayException) >>= \case
    Right a   -> κ a
    Left  msg -> assertFailure msg

mkUnitTest :: (String -> String -> Bool) -> String -> UnitTest -> TestTree
mkUnitTest shouldFail testName UnifierRun{prog, conf, setup = UnifierSetup setup} = 
  -- If only…
  -- https://ghc.haskell.org/trac/ghc/ticket/11350#comment:3
  -- … we could avoid passing this Dict around, and pattern match against h and t directly
  -- instead.
  let go :: forall h t. Dict (Unifier h t) -> TestTree
      go Dict =
        let setupName =  unifierShortName @h <> ";" <> shortName @t in
        let name = testName <> ";" <> setupName in
          (if shouldFail testName setupName then expectFail else id) $
            mkUnifierTest @h @t name conf prog
  in setup go

noUnsolvedProblems :: UnifierProg
noUnsolvedProblems = do
  getUnsolvedProblems >>= \case
    Top   -> return () 
    probs -> failTest =<< do
      probsDoc <- prettyM_ probs
      return$ "Unsolved problems: " PP.<+> probsDoc

someUnsolvedProblems :: UnifierProg
someUnsolvedProblems = do
  getUnsolvedProblems >>= \case
    Top   -> failTest "All problems where unexpectedly solved"
    _     -> return ()

metaEquals :: MonadTerm t m => Meta -> Term t -> m ()
metaEquals mv target = do
 lookupMetaBody mv >>= \case
    Nothing  ->
      failTest =<< do
        mvDoc <- prettyM_ mv
        return$ "Meta" <+>  mvDoc <+> "is not instantiated."
    Just _ -> do
      metaNf   <- nf =<< meta mv []
      targetNf <- nf target
      synEq metaNf targetNf >>= \case
        True   -> return ()
        False  -> failTest =<< do 
          mvDoc       <- prettyM_ mv
          metaNfDoc   <- prettyM_ metaNf
          targetNfDoc <- prettyM_ targetNf
          return$ "Meta" <+> mvDoc <+> " is instantiated to " <+> metaNfDoc <+> ", expected " <+> targetNfDoc <+> "."

metaIsUninstantiated :: MonadTerm t m => Meta -> m ()
metaIsUninstantiated mv = do
  lookupMetaBody mv >>= \case
    Nothing  -> return ()
    Just mvb -> failTest =<< do
      mvDoc <- prettyM_ mv
      mvbDoc <- prettyM_ mvb
      return$ "Meta" <+> mvDoc <+> "is unexpectedly instantiated to " <+> mvbDoc

failTest :: MonadIO m => PP.Doc -> m () 
failTest = liftIO . assertFailure . PP.render

expectTypeCheckerError :: TC t r s () -> TC t r s ()
expectTypeCheckerError m = catchTC m >>= \case
  Right () -> failTest "No exception caught."
  Left{}  -> return ()

type UnitTest = UnifierRun

  
  
