module Issue246-15-9 where

data Nat : Set where
  zero : Nat
  suc  : Nat -> Nat

data Bool : Set where
  true  : Bool
  false : Bool

leq : Nat -> Nat -> Bool
leq zero    _       = true
leq (suc _) zero    = false
leq (suc m) (suc n) = leq m n

ifThenElse : {A : Set} -> Bool -> A -> A -> A
ifThenElse true  a _ = a
ifThenElse false _ b = b

mod : Nat -> Nat -> Nat
mod zero    k = zero
mod (suc n) k = ifThenElse (leq (suc (suc (mod n k))) k)
                           (suc (mod n k))
                           zero

test : mod 
           (suc (suc (suc (suc (suc (suc (suc (suc (suc (suc
           (suc (suc
--           (suc (suc (suc (suc (suc (suc (suc (suc (suc (suc
--           (suc (suc (suc (suc (suc (suc (suc (suc (suc (suc
--           (suc (suc (suc (suc (suc (suc (suc (suc (suc (suc
--           (suc (suc (suc (suc (suc (suc (suc (suc (suc (suc
--
--           (suc (suc (suc (suc (suc (suc (suc (suc (suc (suc
--           (suc (suc (suc (suc (suc (suc (suc (suc (suc (suc
--           (suc (suc (suc (suc (suc (suc (suc (suc (suc (suc
--           (suc (suc (suc (suc (suc (suc (suc (suc (suc (suc
--           (suc (suc (suc (suc (suc (suc (suc (suc (suc (suc
 
           zero


           ))
           ))))))))))

--           ))))))))))
--           ))))))))))
--           ))))))))))
--
--           ))))))))))
--           ))))))))))
--           ))))))))))
--           ))))))))))
--           ))))))))))
--

           (suc (suc (suc (suc (suc (suc (suc (suc (suc
           zero
           )))))))))
           == (suc (suc (suc zero)))
test = refl
