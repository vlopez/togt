module Issue2255 where

{-@AGDA-}
open import Prelude

data Nat : Set where
    zero : Nat
    suc  : Nat -> Nat

foo : Nat -> (Nat -> Nat) -> Nat
foo zero f = f zero
foo (suc n) f = foo n (\n -> f (suc n))

n0 : _
n0 = zero

do10times : (Nat -> Nat) -> Nat -> Nat
do10times f n =  f (f (f (f (f
            (f (f (f (f (f n)))))))))

plus1 : _
plus1 = (\x -> suc x)

plus10 : _
plus10 = do10times plus1

plus100 : _
plus100 = do10times plus10

plus1000 : _
plus1000 = do10times plus100

plus10000 : _
plus10000 = do10times plus1000

n1 : _
n1 = suc zero

n10 : _
n10 = plus10 zero

n100 : _
n100 = plus100 zero

n1000 : _
n1000 = plus1000 zero

n10000 : _
n10000 = plus10000 zero

test2 : foo n1000 (\n -> suc n) == plus1 n1000
test2 = refl
