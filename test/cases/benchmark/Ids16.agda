module Ids16 where

id : {A : Set} -> A -> A
id x = x

id16 : {A : Set} -> A -> A
id16 = id id id id id id id id id id id id id id id id
