module Issue426-big where 

{-@AGDA-}
open import Prelude

data Nat : Set where
    zero : Nat
    suc  : Nat -> Nat

data Bool : Set where
    true  : Bool
    false : Bool

leq : Nat -> Nat -> Bool
leq zero    _       = true
leq (suc _) zero    = false
leq (suc m) (suc n) = leq m n

ifThenElse : {A : Set} -> Bool -> A -> A -> A
ifThenElse true  a _ = a
ifThenElse false _ a = a

mod : Nat -> Nat -> Nat
mod zero    k = zero
mod (suc n) k = ifThenElse (leq (suc r) k) r zero 
  where 
    r : _
    r = suc (mod n k)

n0 : _
n0 = zero

do10times : (Nat -> Nat) -> Nat -> Nat
do10times f n =  f (f (f (f (f
            (f (f (f (f (f n)))))))))

plus1 : _
plus1 = (\x -> suc x)

plus10 : _
plus10 = do10times plus1

n1 : _
n1 = suc zero

n10 : _
n10 = plus10 zero

n100 : _
n100 = do10times plus10 zero

test : mod (plus1 n100) n10 == n1
test = refl
