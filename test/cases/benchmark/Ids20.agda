module Ids20 where

id : {A : Set} -> A -> A
id x = x

id20 : {A : Set} -> A -> A
id20 = (id id id id id id id id id id 
        id id id id id id id id id id)
