module Ids12 where

id : {A : Set} -> A -> A
id x = x

id12 : {A : Set} -> A -> A
id12 = id id id id id id id id id id id id
