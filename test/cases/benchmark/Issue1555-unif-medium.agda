{-# OPTIONS --type-in-type #-}
module Issue1555-unif-medium where

record Unit : Set where
  constructor unit

data Bool : Set where
  true  : Bool
  false : Bool

ifThenElse : {A : Set} -> Bool -> A -> A -> A
ifThenElse true  x _ = x
ifThenElse false _ y = y

test : {
  _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _
  : Unit} -> (b : Bool) -> _
test2 : (b : Bool) -> Unit

test true  = unit
test false = unit

test2 = test



