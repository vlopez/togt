{-# OPTIONS --type-in-type #-}
module J_red_inv where

postulate A : Set
postulate a : A
postulate x : A
postulate f : A -> A

postulate subst : (y : A) (P : A -> Set) -> P x -> P y

postulate Q : A -> A -> Set
postulate Qx : Q a x

cong2 : (y : A) -> Q a y
cong2 y = subst y (\ z -> Q _ z) Qx
