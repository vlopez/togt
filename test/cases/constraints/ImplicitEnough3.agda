module ImplicitEnough2 where

postulate F : Set -> Set
postulate f : (A : Set) -> F A

postulate A : Set
postulate B : Set

postulate t : {M : Set -> Set -> Set} -> (x y z : Set) -> F (M x x) -> F (M y z) -> Set

-- TODO: Use postulates instead of variables
a : (x : Set) -> Set
a x = t x x B (f x) (f B)
