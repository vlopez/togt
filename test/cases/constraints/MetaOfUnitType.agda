{-# OPTIONS --type-in-type --without-K #-}
module HierReduced_mini_red where

{-@AGDA-}
open import Prelude

data Ki : Set where
  set : Ki
  prop : Ki

record One : Set where
  constructor tt

data Zero : Set where {-@EMPTY-}

isSet : Ki -> Set
isSet set  = One
isSet prop = Zero

data UpU (k : Ki) : Set where
  Prf' : {q : isSet k}(P : UpU prop) -> UpU k

raise : Ki -> Set
raise k = UpU k

PRF : raise prop -> raise set
PRF P = Prf' P
