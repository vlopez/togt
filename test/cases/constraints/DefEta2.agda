module DefEta2 where

{-@AGDA-}
open import Prelude

postulate A : Set

f : A -> A -> A
f x y = y

postulate a : A
postulate b : A

testEq : f a == f b
testEq = refl
