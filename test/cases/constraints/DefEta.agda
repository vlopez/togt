module DefEta where

{-@AGDA-}
open import Prelude

postulate A : Set

f : A -> A
f x = x

g : A -> A
g x = x

testEq : f == g
testEq = refl
