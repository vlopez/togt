{-# OPTIONS --type-in-type --without-K #-}
module HierReduced_mini where

{-@AGDA-}
open import Prelude

record LEVEL : Set where
  constructor level
  field
    uni  : Set
{-@AGDA-}
open LEVEL public

data UpU (l : LEVEL) : Set where
  U' : UpU l

