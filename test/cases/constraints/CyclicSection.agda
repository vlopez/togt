module CyclicSection where

{-@AGDA-}
open import Prelude

data A : Set where
  a : A

data Box : Set where
  box : A -> Box

unbox : Box -> A
unbox (box x) = x

mutual
  X : Box
  X = _
 
  test1 : box (unbox X) == X
  test1 = refl

  test2 : X == box a 
  test2 = refl
