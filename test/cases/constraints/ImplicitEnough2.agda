module ImplicitEnough2 where

postulate F : Set -> Set
postulate f : (A : Set) -> F A

postulate A : Set
postulate B : Set

postulate t : {M : Set -> Set -> Set} -> (x y z : Set) -> F (M x x) -> F (M y z) -> Set

-- TODO: Use postulates instead of variables
a : (x : Set) (y : Set) -> Set
a x y = t x x y (f x) (f y)
