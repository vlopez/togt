module Omega2 where

{-@AGDA-}
open import Tog

omega : _
omega = (\x -> x x)

p : omega omega == omega
p = refl
