module PruneTermArg2 where

record Unit : Set where
  constructor tt
  
postulate F : Set -> Set 
postulate a : Set -> Unit -> Set

postulate kA : {M : Set -> Set -> Set} -> 
               ((x : Set) (y : Unit) -> F (M x (a x y))) -> 
               Set


postulate kB : {M : Set -> Set -> Set} -> 
              ((x : Set) (y : Unit) -> F (M x (a x y))) -> 
              ((x : Set) (y : Set) -> F (M x y)) -> 
              Set

postulate F' : (A : Set) -> F A

-- Some problems
-- ?M : Set -> Set -> Set

---------------------------------
K1 : Set
K1 = kA (\x y -> F' (a x tt))
---------------------------------
-- Unification problem: ∀(x:Set)(y:Unit). ?M x (a x y) ≈ a x tt
-- Agda solution:       ?M := \x _ -> a x tt 
-- Expected:            No solution, because (?M := \_ y -> y is also a solution)
---------------------------------

---------------------------------
K2 : Set
K2 = kB (\x y -> F' (a x tt))
        (\x y -> F' (a x tt))
---------------------------------
-- Unification problem:  ∀(x:Set)(y:Unit). ?M x (a x y) ≈ a x tt
--                     ∧ ∀(x:Set)(y:Set).  ?M x y       ≈ a x tt
-- Agda solution:        ?M := \x _ -> a x tt
-- Expected solution:       [idem.]
--------------------------------

--------------------------------
K3 : Set
K3 = kB {\x y -> y}
        (\x y -> F' (a x tt))
        (\x y -> F' y)
--------------------------------
-- Unification problem:  ∀(x:Set)(y:Unit). (\x y -> y) x (a x y) ≈ a x tt
--                     ∧ ∀(x:Set)(y:Set).  (\x y -> y) x y       ≈ y
-- Agda solution:        OK
-- Expected solution:    OK
--------------------------------

--------------------------------
K4 : Set
K4 = kB (\x y -> F' (a x tt))
        (\x y -> F' y)
--------------------------------
-- Unification problem:   ∀(x:Set)(y:Unit). ?M x (a x y) ≈ a x tt
--                      ∧ ∀(x:Set)(y:Set).  ?M x y       ≈ y
-- Agda solution:        ERROR (y ≠ a x tt)
-- Expected solution:    ?M := \x y -> y   (cf. `K3`)
--------------------------------
