module Omega where

{-@AGDA-}
open import Tog

p : (\x -> x x) (\x -> x x) == (\x -> x x) (\x -> x x)
p = refl
