module ThesisUnsound where

postulate Nat : Set
postulate zero : Nat

coerce : (F : Nat -> Set) -> F zero -> F zero
coerce = (\ F x -> x)

omega : (Nat -> Nat) -> Nat
omega = \ x -> x (coerce _ x)

Omega : Nat
Omega = omega (coerce _ omega)


