module Omega2 where

{-@AGDA-}
open import Tog

omega : _
omega = (\x -> x x)

p : omega omega == (\x -> x)
p = refl
