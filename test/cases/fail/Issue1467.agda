module Issue1497 where

record Times (A : Set) (B : Set) : Set where
  constructor pair
  field
    fst : A
    snd : B

app : {A : Set} {B : Set} -> Times (A -> B) (A -> B)
app (pair f x) = f x

data D : Set where 
    d : D

postulate P   : {A : Set} -> A -> Set
postulate p   : (f : D -> D) -> P f -> P (f d)
postulate foo : (F : Set -> Set) -> F D
postulate bar : (F : Set -> Set) -> P (foo F)

mutual
   H : Set -> Set
   H = _

   q : P (app (foo (\ A -> Times (A -> A) A)))
   q = p (foo H) (bar H)
