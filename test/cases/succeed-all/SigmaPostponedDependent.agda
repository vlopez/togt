module SigmaPostponedDependent where

{-@AGDA-}
open import Prelude

--data _==_ {A : Set}(x : A) : A -> Set where
--  refl : x == x

data Bool : Set where
  true : Bool
  false : Bool

data Bool2 (b : Bool) : Set where
  trueTrue   : true == b -> Bool2 b
  falseFalse : false == b -> Bool2 b

record Sigma (A : Set) (B : A -> Set) : Set
record Sigma A B where
  constructor pair
  field
    fst : A
    snd : B fst

{-@AGDA-}
open Sigma

postulate f : (A : Set) -> A -> Bool

Foo : Bool -> Set
Foo true = Bool
Foo false = Bool

Alpha : Bool
Beta : Foo Alpha

Bar : Bool -> Set
Bar = _

test : f (Sigma Bool (\_ -> Foo Alpha)) (pair Alpha Beta) == f (Sigma Bool Bar) (pair false false)

Alpha = _
Beta = Alpha
test = refl

-- Normalizing Alpha yields a metavariable, but normalizing Beta yields
-- false.
