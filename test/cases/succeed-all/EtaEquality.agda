module EtaEquality where

{-@AGDA-}
open import Tog

postulate A : Set
postulate B : Set
postulate f : A -> B

p1 : (\ x ->  f x) == f
p1 = refl

record Pair : Set where
  constructor pair
  field
    fst : A
    snd : B

{-@AGDA-}
open Pair


p2 : {x : Pair} -> (pair (fst x) (snd x)) == x
p2 = refl

