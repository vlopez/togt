{-# OPTIONS --type-in-type #-}
module Nested where

{-@AGDA-}
open import Prelude

record Sigma1 (A : Set)(B : A -> Set) : Set
record Sigma1 A B where
  constructor pair
  field
    fst : A
    snd : B fst

{-@AGDA-}
open Sigma1

record Sigma (A : Set)(B : A -> Set) : Set
record Sigma A B where
  constructor inn
  field
    out : Sigma1 A B

{-@AGDA-}
open Sigma

data Unit : Set
data Unit where
  tt : Unit

Cat : Set
Cat =
  Sigma Set                                                  (\ Obj ->
  Sigma (Obj -> Obj -> Set)                                  (\ Hom ->
  Sigma ((X : _) -> Hom X X)                                 (\ id ->
  Unit)))

Obj : (C : Cat) -> Set
Obj C = fst (out C)

Hom : (C : Cat) -> Obj C -> Obj C -> Set
Hom C = fst (out (snd (out C)))

id : (C : Cat) -> (X : _) -> Hom C X X
id C = fst (out (snd (out (snd (out C)))))
