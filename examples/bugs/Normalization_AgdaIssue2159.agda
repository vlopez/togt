module Normalization_AgdaIssue2147 where

data Nat : Set where
  zero : Nat
  suc : Nat -> Nat

data AB : Set where
  A : AB
  B : AB

foo : Nat -> AB -> AB
foo zero t = A
foo (suc n) t = foo n A -- NB tail-recursive

test : foo 10000000 A == A
test = refl -- memory blows up here
