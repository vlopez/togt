module RecMeta where

{-@AGDA-}
open import Tog

data Nat : Set where
  zero : Nat
  suc  : Nat -> Nat

postulate A : Set
postulate box : A -> A

postulate equate : (A : Set) (a : A) -> (b : A) -> a == b -> A

mutual
  q : Nat
  q = _

  f : Nat

  h : Nat
  h = equate Nat f q refl

  f = suc q

data Unit : Set where
  unit : Unit

data B : Set where
  inn : A -> B

out : B -> A
out (inn a) = a

postulate P : (y : A) (z : Unit -> B) -> Set
postulate p : (x : Unit -> B) -> P (out (x unit)) x

mutual
  d : Unit -> B
  d unit = inn _           -- Y

  g : P (out (d unit)) d
  g = p _                  -- X

-- Agda solves  d unit = inn (out (d unit))
--
-- out (X unit) = out (d unit) = out (inn Y) = Y
-- X = d

