module WellFounded where

{-@AGDA-}
open import Prelude

data Bit : Set where
  B0 : Bit
  B1 : Bit

data Bin : Set where
  I  : Bin
  snoc : Bit -> Bin -> Bin

data Bot : Set where
  {-@EMPTY-}

one : Bin
two : Bin

one = I
two = snoc B0 I

data LT (a : Bin) (b : Bin) : Set where
  pO : {d : Bit} { x : Bin } -> a == I -> b == snoc d x -> LT a b
  pE : { x : Bin } -> a == snoc B0 x -> b == snoc B1 x -> LT a b

  pS : { d : Bit } { e : Bit } { x y : Bin } ->
       LT x y ->
       a == snoc d x ->
       b == snoc e y ->
       LT a b

inc : Bin -> Bin
inc I = snoc B0 I
inc (snoc B0 a) = snoc B1 a
inc (snoc B1 a) = snoc B0 (inc a)

p-inc : (a : Bin) -> LT a (inc a)
p-inc I = pO refl refl
p-inc (snoc B0 a) = pE refl refl
p-inc (snoc B1 a) = pS (p-inc a) refl refl

-- Tog: TODO: Flexible variables
-- sym : {A : Set} -> {a : A} {b : A} -> a == b -> b == a
-- sym refl = refl

no-LT-I : {A : Set} -> (a : Bin) -> LT a I -> A
no-LT-I I (pO refl ())
no-LT-I I (pE () y)
no-LT-I I (pS p () y)
no-LT-I (snoc x y) (pO () q)
no-LT-I (snoc x y) (pE p ())
no-LT-I (snoc x y) (pS p q ())

dec : (a : Bin) -> LT I a -> Bin
dec I p = no-LT-I I p
dec (snoc B0 I) p = I
dec (snoc B0 (snoc x y)) p = snoc B1 (dec (snoc  x y) (pO refl refl))
dec (snoc B1 x) p = snoc B0 x

p-dec : (a : Bin) -> (p : LT I a) -> LT (dec a p) a
p-dec I p = no-LT-I I p
p-dec (snoc B0 I) p = p
p-dec (snoc B0 (snoc x a)) p = pS (p-dec (snoc x a) (pO refl refl)) refl refl
p-dec (snoc B1 a) p = pE refl refl

data Acc-LT (x : Bin) : Set where
  acc : ((y : Bin) -> LT y x -> Acc-LT y) -> Acc-LT x

Acc-LT' : (x : Bin) -> Set
Acc-LT' x = (y : Bin) -> LT y x -> Acc-LT y

acc-lt-I : Acc-LT' I
acc-lt-I y p = no-LT-I y p

WF-LT' : (x : Bin) -> Acc-LT' x
WF-LT  : (x : Bin) -> Acc-LT  x

WF-LT' x I p = acc acc-lt-I
WF-LT' x (snoc b y) p = {!!}

WF-LT x = acc (WF-LT' x)
