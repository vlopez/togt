module Normalization_AgdaIssue2147 where

{-@AGDA-}
open import Prelude

data Nat : Set where
  Z : Nat
  S : Nat -> Nat

data BNat (n : Nat) : Set where
  one         : BNat n
  one-p-twice : {m : Nat} -> m == S n -> BNat m -> BNat n
  twice       : BNat -> BNat

two : BNat
two = twice one

inc : BNat -> BNat
inc one = two
inc (one-p-twice x) = twice (inc x)
inc (twice x) = one-p-twice x

dec : BNat -> BNat
dec one = one
dec (twice one) = one
dec (twice x)   = one-p-twice (dec x)
dec (one-p-twice x) = twice x

data T : Set where
    tt : T

kibi : BNat -> BNat
kibi x = twice (twice (twice (twice (twice (
          twice (twice (twice (twice (twice (
          x
          )))))
          )))))

sixteen : BNat -> BNat
sixteen x = twice (twice (twice (twice (
           x
           ))))

mibi : BNat -> BNat
mibi x = (kibi (kibi x))

-- foo : BNat -> T -> T
-- foo one  tt = tt
-- foo (twice x) tt = foo x (foo x tt)
-- foo (one-p-twice x) tt =foo x (foo x tt)

-- test : foo (kibi (kibi one)) tt == tt
-- test = refl -- memory blows up here



-- foo : Nat -> T -> T
-- foo 0 tt = tt
-- foo (suc n) tt = foo n tt -- NB tail-recursive
--

-- plus : BNat -> BNat -> BNat
-- plus-carry : BNat -> BNat -> BNat
--
-- plus one x = inc x
-- plus x one = inc x
-- plus (one-p-twice x) (one-p-twice y) = twice (plus-carry x y)
-- plus (one-p-twice x) (twice y) = one-p-twice (plus x y)
-- plus (twice x) (one-p-twice y) = one-p-twice (plus x y)
-- plus (twice x) (twice y) = twice (plus x y)
--
-- plus-carry one x = inc x
-- plus-carry x one = inc x
-- plus-carry (one-p-twice x) (one-p-twice y) = one-p-twice (plus-carry x y)
-- plus-carry (one-p-twice x) (twice y) = twice (plus-carry x y)
-- plus-carry (twice x) (one-p-twice y) = twice (plus-carry x y)
-- plus-carry (twice x) (twice y) = one-p-twice (plus x y)

