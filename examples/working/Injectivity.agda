module Injectivity where

{-@AGDA-}
open import Tog

data Bool : Set where
    true : Bool
    false : Bool

postulate f : Bool -> Bool

a : Bool
a = _

p : f a == f false
p = refl
