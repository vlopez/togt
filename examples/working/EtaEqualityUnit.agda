module EtaEqualityUnit where

{-@AGDA-}
open import Tog

record Unit : Set where
  constructor tt

postulate s : Unit
postulate t : Unit

p : s == t
p = refl
