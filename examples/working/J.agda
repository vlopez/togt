{-# OPTIONS --type-in-type #-}
module Vec_mini where

{-@AGDA-}
open import Prelude

-- Properties of equality.

subst : {A : Set} {x y : A} (P : A -> Set) ->
        x == y -> P x -> P y
subst P = J (\ x y _ -> P x -> P y) (\ x p -> p)

sym : {A : Set} {x : A} {y : A} -> x == y -> y == x
sym {A} {x} {y} p = subst (\ y -> y == x) p refl

trans : {A : Set} {x : A} {y : A} {z : A} -> x == y -> y == z -> x == z
trans {A} {x} p q = subst (\ y -> x == y) q p

cong : {A B : Set} {x : A} {y : A} (f : A -> B) -> x == y -> f x == f y
cong f p = subst (\ y -> f _ == f y) p refl
