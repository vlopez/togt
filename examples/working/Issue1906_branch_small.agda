module Issue1906_branch where

data Bool : Set where
  true : Bool
  false : Bool

data F (n : Bool) : Set where
  f : F n

module B (a : Bool) where
  b1 : F a
  b1 = f

xor : Bool -> Bool -> Bool
xor true true = false
xor false true = true
xor true false = true
xor false false = false

mash : {a : Bool} {b : Bool} -> F a -> F b -> F (xor a b)
mash f f = f

p1 : F false
p1 = a
  where
    open B _

    a0 : F _
    a0 = b1

    a1 : F _
    a1 = a0
    a2 : F _
    a2 = mash a1 a0
    a3 : F _
    a3 = mash a2 a1
    a4 : F _
    a4 = mash a3 a2
    a : F _
    a = a0
