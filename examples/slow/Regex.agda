module Regex where

{-@AGDA-}
open import Tog

data Bool : Set where
  true : Bool
  false : Bool

ifThenElse : {P : Set} -> (b : Bool) -> P -> P -> P
ifThenElse true  t _ = t
ifThenElse false _ f = f

data List (T : _) : Set where
  nil : List T
  cons : T -> List T -> List T

data regex (T : Set) : Set where
  empty   : regex T
  epsilon : regex T
  lit     : T -> regex T
  concat  : regex T -> regex T -> regex T
  union   : regex T -> regex T -> regex T
  star    : regex T -> regex T   

and : Bool -> Bool -> Bool
and true  a = a
and false a = false

or : Bool -> Bool -> Bool
or true  a = true
or false a = a

equals : Bool -> Bool -> Bool
equals true  true = true
equals false true = false
equals true false = false
equals false false = true

hasEpsilon : {T : Set} -> regex T -> Bool
hasEpsilon empty = false
hasEpsilon epsilon = true
hasEpsilon (lit _) = false
hasEpsilon (concat a b) = and (hasEpsilon a) (hasEpsilon b)
hasEpsilon (union  a b) = or  (hasEpsilon a) (hasEpsilon b)
hasEpsilon (star _) = true

deriv : {T : Set} (eq : T -> T -> Bool) -> T -> regex T -> regex T
deriv _ _ empty   = empty
deriv _ _ epsilon = empty
deriv eq t (lit t') = ifThenElse (eq t t') epsilon empty
deriv eq t (concat a b) = union (concat (deriv eq t a) b)
                                (ifThenElse (hasEpsilon a)
                                            (concat a (deriv eq t b))
                                            empty)
deriv eq t (union a b) = union (deriv eq t a) (deriv eq t b)
deriv eq t (star a) = concat (deriv eq t a) (star a)


match : {T : Set} (eq : T -> T -> Bool) -> regex T -> List T -> Bool
match _ re nil = hasEpsilon re
match eq re (cons a as) = match eq (deriv eq a re) as


one_length : regex Bool
one_length = union (lit true) (lit false)

odd_length : regex Bool
odd_length = concat (one_length) (star (concat one_length one_length)) 


ex1 : List Bool
ex1 = cons true (
      cons true (cons false (cons true (cons false (cons true (cons false (
      cons true (cons false (cons true (cons false (cons true (cons false (
      nil
      )
      ))))))
      ))))))


p1 : match equals odd_length ex1 == true
p1 = refl
