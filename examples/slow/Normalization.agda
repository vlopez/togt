module Normalization where

{-@AGDA-}
open import Tog

data Maybe (A : Set) : Set where
  nothing : Maybe A
  just    : A -> Maybe A

data Nat : Set where
  zero : Nat
  suc  : Nat -> Nat

data Unit : Set where
  tt : Unit

data Empty : Set where
  {-@EMPTY-}

add : Maybe Unit -> Maybe Unit -> Maybe Unit
add nothing nothing = nothing
add nothing (just tt) = just tt
add (just tt) nothing = just tt
add (just tt) (just tt) = just tt

f : Nat -> Maybe Unit
f zero = nothing
f (suc zero) = just tt
f (suc n) = add (f n) (f n)

big : Nat
big =    suc (suc (suc (suc (suc (
         suc (suc (suc (suc (suc (
         suc (suc (suc (suc (suc (
         suc (suc (suc (suc (suc (
         suc (suc (suc (suc (suc (
         suc (suc (suc (suc (suc (
         suc (suc (suc (suc (suc (
         suc (suc (suc (suc (suc (
         suc (suc (suc (suc (suc (
         suc (suc (suc (suc (suc (
         suc (suc (suc (suc (suc (
         suc (suc (suc (suc (suc (
         suc (suc (suc (suc (suc (
         suc (suc (suc (suc (suc (
         suc (suc (suc (suc (suc (
         suc (suc (suc (suc (suc (



         zero

         )))))
         )))))
         )))))
         )))))
         )))))
         )))))
         )))))
         )))))
         )))))
         )))))
         )))))
         )))))
         )))))
         )))))
         )))))
         )))))


foo : (f zero == f big) -> Empty
foo ()
