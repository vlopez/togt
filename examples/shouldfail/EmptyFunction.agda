module EmptyFunction where

{-@AGDA-}
open import Tog

postulate A : Set

data Nat : Set where
  zero : Nat
  suc  : Nat -> Nat

f : Nat -> A

p : f zero == f _
p = refl
