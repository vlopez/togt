-- This Happy file was machine-generated by the BNF converter
{
{-# OPTIONS_GHC -fno-warn-incomplete-patterns -fno-warn-overlapping-patterns #-}
module Tog.Raw.Par where
import Tog.Raw.Abs
import Tog.Raw.Lex
import Tog.Raw.ErrM

}

%name pModule Module
%name pExpr Expr
-- no lexer declaration
%monad { Err } { thenM } { returnM }
%tokentype {Token}
%token
  '(' { PT _ (TS _ 1) }
  ')' { PT _ (TS _ 2) }
  '->' { PT _ (TS _ 3) }
  '.' { PT _ (TS _ 4) }
  ':' { PT _ (TS _ 5) }
  ';' { PT _ (TS _ 6) }
  '=' { PT _ (TS _ 7) }
  '==' { PT _ (TS _ 8) }
  '\\' { PT _ (TS _ 9) }
  'constructor' { PT _ (TS _ 10) }
  'data' { PT _ (TS _ 11) }
  'field' { PT _ (TS _ 12) }
  'import' { PT _ (TS _ 13) }
  'module' { PT _ (TS _ 14) }
  'mutual' { PT _ (TS _ 15) }
  'open' { PT _ (TS _ 16) }
  'postulate' { PT _ (TS _ 17) }
  'public' { PT _ (TS _ 18) }
  'record' { PT _ (TS _ 19) }
  'where' { PT _ (TS _ 20) }
  '{' { PT _ (TS _ 21) }
  '{-@AGDA-}' { PT _ (TS _ 22) }
  '{-@EMPTY-}' { PT _ (TS _ 23) }
  '}' { PT _ (TS _ 24) }

L_Name { PT _ (T_Name _) }
L_Empty { PT _ (T_Empty _) }


%%

Name    :: { Name} : L_Name { Name (mkPosToken $1)}
Empty    :: { Empty} : L_Empty { Empty (mkPosToken $1)}

Module :: { Module }
Module : 'module' Name Params 'where' '{' ListDecl '}' { Tog.Raw.Abs.Module $2 $3 $6 }
QName :: { QName }
QName : QName '.' Name { Tog.Raw.Abs.Qual $1 $3 }
      | Name { Tog.Raw.Abs.NotQual $1 }
ListDecl :: { [Decl] }
ListDecl : {- empty -} { [] }
         | Decl { (:[]) $1 }
         | Decl ';' ListDecl { (:) $1 $3 }
Decl :: { Decl }
Decl : TypeSig { Tog.Raw.Abs.TypeSig $1 }
     | Name ListPattern FunDefBody { Tog.Raw.Abs.FunDef $1 (reverse $2) $3 }
     | 'data' Name Params DataBody { Tog.Raw.Abs.Data $2 $3 $4 }
     | 'record' Name Params RecordBody { Tog.Raw.Abs.Record $2 $3 $4 }
     | 'open' 'import' Import ListOpenSpec { Tog.Raw.Abs.OpenImport $3 (reverse $4) }
     | 'open' Import ListOpenSpec { Tog.Raw.Abs.OpenApp $2 (reverse $3) }
     | 'import' Import { Tog.Raw.Abs.Import $2 }
     | 'module' Name '=' Import { Tog.Raw.Abs.ModuleApp $2 $4 }
     | 'postulate' '{' ListTypeSig '}' { Tog.Raw.Abs.Postulate $3 }
     | 'mutual' '{' ListDecl '}' { Tog.Raw.Abs.Mutual $3 }
     | Module { Tog.Raw.Abs.Module_ $1 }
     | '{-@AGDA-}' { Tog.Raw.Abs.Pragma }
ListOpenSpec :: { [OpenSpec] }
ListOpenSpec : {- empty -} { [] }
             | ListOpenSpec OpenSpec { flip (:) $1 $2 }
OpenSpec :: { OpenSpec }
OpenSpec : 'public' { Tog.Raw.Abs.Public }
ListTypeSig :: { [TypeSig] }
ListTypeSig : {- empty -} { [] }
            | TypeSig { (:[]) $1 }
            | TypeSig ';' ListTypeSig { (:) $1 $3 }
Import :: { Import }
Import : QName { Tog.Raw.Abs.ImportNoArgs $1 }
       | QName ListArg { Tog.Raw.Abs.ImportArgs $1 $2 }
TypeSig :: { TypeSig }
TypeSig : Name ':' Expr { Tog.Raw.Abs.Sig $1 $3 }
Where :: { Where }
Where : 'where' '{' ListDecl '}' { Tog.Raw.Abs.Where $3 }
      | {- empty -} { Tog.Raw.Abs.NoWhere }
Params :: { Params }
Params : {- empty -} { Tog.Raw.Abs.NoParams }
       | ListBinding { Tog.Raw.Abs.ParamDecl $1 }
       | ListHiddenName { Tog.Raw.Abs.ParamDef $1 }
HiddenName :: { HiddenName }
HiddenName : Name { Tog.Raw.Abs.NotHidden $1 }
           | '{' Name '}' { Tog.Raw.Abs.Hidden $2 }
ListHiddenName :: { [HiddenName] }
ListHiddenName : HiddenName { (:[]) $1 }
               | HiddenName ListHiddenName { (:) $1 $2 }
DataBody :: { DataBody }
DataBody : ':' Name { Tog.Raw.Abs.DataDecl $2 }
         | 'where' Constrs { Tog.Raw.Abs.DataDef $2 }
         | ':' Name 'where' Constrs { Tog.Raw.Abs.DataDeclDef $2 $4 }
Constrs :: { Constrs }
Constrs : '{' ListConstr '}' { Tog.Raw.Abs.Constrs $2 }
        | '{' '{-@EMPTY-}' '}' { Tog.Raw.Abs.NoConstrs }
RecordBody :: { RecordBody }
RecordBody : ':' Name { Tog.Raw.Abs.RecordDecl $2 }
           | 'where' '{' 'constructor' Name Fields '}' { Tog.Raw.Abs.RecordDef $4 $5 }
           | ':' Name 'where' '{' 'constructor' Name Fields '}' { Tog.Raw.Abs.RecordDeclDef $2 $6 $7 }
Fields :: { Fields }
Fields : {- empty -} { Tog.Raw.Abs.NoFields }
       | ';' 'field' '{' ListConstr '}' { Tog.Raw.Abs.Fields $4 }
Constr :: { Constr }
Constr : Name ':' Expr { Tog.Raw.Abs.Constr $1 $3 }
ListConstr :: { [Constr] }
ListConstr : {- empty -} { [] }
           | Constr { (:[]) $1 }
           | Constr ';' ListConstr { (:) $1 $3 }
FunDefBody :: { FunDefBody }
FunDefBody : {- empty -} { Tog.Raw.Abs.FunDefNoBody }
           | '=' Expr Where { Tog.Raw.Abs.FunDefBody $2 $3 }
Telescope :: { Telescope }
Telescope : ListBinding { Tog.Raw.Abs.Tel $1 }
Binding :: { Binding }
Binding : '(' ListArg ':' Expr ')' { Tog.Raw.Abs.Bind $2 $4 }
        | '{' ListArg ':' Expr '}' { Tog.Raw.Abs.HBind $2 $4 }
ListBinding :: { [Binding] }
ListBinding : Binding { (:[]) $1 }
            | Binding ListBinding { (:) $1 $2 }
Expr :: { Expr }
Expr : '\\' ListName '->' Expr { Tog.Raw.Abs.Lam $2 $4 }
     | Telescope '->' Expr { Tog.Raw.Abs.Pi $1 $3 }
     | Expr1 '->' Expr { Tog.Raw.Abs.Fun $1 $3 }
     | Expr1 { $1 }
Expr1 :: { Expr }
Expr1 : Expr2 '==' Expr2 { Tog.Raw.Abs.Eq $1 $3 } | Expr2 { $1 }
Expr2 :: { Expr }
Expr2 : ListArg { Tog.Raw.Abs.App $1 }
Expr3 :: { Expr }
Expr3 : '(' Expr ')' { $2 } | QName { Tog.Raw.Abs.Id $1 }
Arg :: { Arg }
Arg : '{' Expr '}' { Tog.Raw.Abs.HArg $2 }
    | Expr3 { Tog.Raw.Abs.Arg $1 }
ListArg :: { [Arg] }
ListArg : Arg { (:[]) $1 } | Arg ListArg { (:) $1 $2 }
Pattern :: { Pattern }
Pattern : Empty { Tog.Raw.Abs.EmptyP $1 }
        | '(' QName ListPattern ')' { Tog.Raw.Abs.ConP $2 (reverse $3) }
        | QName { Tog.Raw.Abs.IdP $1 }
        | '{' Pattern '}' { Tog.Raw.Abs.HideP $2 }
ListPattern :: { [Pattern] }
ListPattern : {- empty -} { [] }
            | ListPattern Pattern { flip (:) $1 $2 }
ListName :: { [Name] }
ListName : Name { (:[]) $1 } | Name ListName { (:) $1 $2 }
{

returnM :: a -> Err a
returnM = return

thenM :: Err a -> (a -> Err b) -> Err b
thenM = (>>=)

happyError :: [Token] -> Err a
happyError ts =
  Bad $ "syntax error at " ++ tokenPos ts ++ 
  case ts of
    [] -> []
    [Err _] -> " due to lexer error"
    t:_ -> " before `" ++ id(prToken t) ++ "'"

myLexer = tokens
}

