bnfc_output = $(patsubst %,bnfc/src/Tog/Raw/%,Abs.hs ErrM.hs Layout.hs Print.hs Lex.x Par.y)
hs_sources = $(shell find src/ -name '[A-Z]*.hs')
alex_file = bnfc/src/Tog/Raw/Lex
happy_file = bnfc/src/Tog/Raw/Par
executable = dist/build/tog/tog
STACK_BUILD_OPTS = --ghc-options "-rtsopts -O2 -j4"

.PHONY: build
build: $(executable)

$(bnfc_output): src/Tog/Raw/Raw.cf bnfc/fix-warnings.sh
	-@rm $(bnfc_output)
	@mkdir -p bnfc/src
	@(cd bnfc/src && stack exec -- bnfc -p Tog -d ../../$<)
	#@(cd bnfc/ && ./fix-warnings.sh)

$(alex_file).hs: $(alex_file).x
	alex $<

$(happy_file).hs: $(happy_file).y
	happy $<

$(executable): $(bnfc_output) $(hs_sources) tog.cabal
	stack build $(STACK_BUILD_OPTS)

.PHONY: bnfc
bnfc: $(bnfc_output)

.PHONY: clean
clean:
	rm -rf bnfc/src
	stack clean tog

.PHONY: test
test: $(executable)
	stack exec -- time ./test

modules.pdf: $(bnfc_output) $(hs_sources)
	graphmod -i src -i bnfc src/Tog/Main.hs | dot -T pdf -o modules.pdf

.PHONY: install-prof
install-prof: $(bnfc_output) $(hs_sources)
	stack install --library-profiling --executable-profiling --ghc-options -rtsopts

.PHONY: install
install: $(bnfc_output) $(hs_source)
        #	make tags
	stack install $(STACK_BUILD_OPTS)

.PHONY: ghci
ghci: $(bnfc_output) $(alex_file).hs $(happy_file).hs
	stack ghci tog:lib tog:tog

.PHONY: tags
tags:
	hasktags --etags src

.PHONY: build-all
build-all:
	stack build --test --no-run-tests --bench --no-run-benchmarks
