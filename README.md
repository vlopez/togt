# Tog⁺ - A prototypical implementation of dependent types

Tog⁺ (pronounced tog-tee) is right now a laboratory to experiment in
implementing dependent types.

It is a variant/extension of [tog][4].

## Installation

Before continuing, install [BNFC-2.8.1][1], and recent versions of
[alex][2] and [happy][3]. Tested with Alex 3.1.7 and Happy 1.19.5.

    cabal install BNFC-2.8.1 alex-3.1.7 happy-1.19.5

Then, run:

    git clone https://framagit.org/vlopez/togt
    cd tog
    make

If you want to install the binary

    make install

## Usage

To type check files

    tog [FILE]

`tog --help` gives the full options.

See `examples/` for some example files, it's basically a simple `Agda`.

## Tests

To run the (sadly quite limited) tests, run

    make test

## Module structure

See the exported modules in the library section `tog.cabal`, each of
them should contain a brief description.  `Tog.Main` is the module that
defines main function for the `tog` executable.

[1]: https://hackage.haskell.org/package/BNFC-2.8.1
[2]: https://hackage.haskell.org/package/alex-3.1.7
[3]: https://hackage.haskell.org/package/happy-1.19.5
[4]: https://github.com/bitonic/tog
